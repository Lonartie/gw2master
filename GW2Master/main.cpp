#include "stdafx.h"
#include "Dashboard.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dashboard::Instance().show();
    return a.exec();
}
