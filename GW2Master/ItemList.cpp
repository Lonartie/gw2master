#include "stdafx.h"
#include "ItemList.h"
#include "GW2Master.Core/API.h"
using namespace GW2API;

ItemList::ItemList(QWidget* parent)
   : QWidget(parent)
{
   ui.setupUi(this);
   installEventFilter(this);
   ui.list->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
   clearList();
   fillList();
   ui.progress->setMaximum(100);
}

void ItemList::setVisibleData(ItemData data)
{
   this->data = data;
}

bool ItemList::eventFilter(QObject* watched, QEvent* event)
{
   if (event->type() == QEvent::Close)
      emit closing();
   return false;
}

void ItemList::fillList()
{
   current = 0;
   QStringList headers;
   if (contains(ID)) headers << "ID";
   if (contains(Image)) headers << "Image";
   if (contains(Name)) headers << "Name";
   if (contains(VendorValue)) headers << "Vendor value";
   ui.list->setColumnCount(headers.size());
   ui.list->setHorizontalHeaderLabels(headers);
   //ui.list->setUpdatesEnabled(false);

   auto th = Item::AllInitializedAsync([this](auto& i, auto max) 
   {
      //waiter.lock();
      //std::this_thread::sleep_for(std::chrono::milliseconds(50));
      API::Enqueue(std::bind(&ItemList::addItem, this, i, max), this);
      //waiter.unlock();
   });

   connect(th, &QThread::finished, this, [=]() { th->deleteLater(); });
}

void ItemList::clearList() const
{
   ui.list->clear();
}

bool ItemList::contains(ItemData data) const
{
   return (((int) this->data & (int) data) == ((int) data));
}

void ItemList::addItem(const Item& item, std::size_t max)
{
   current++;
   ui.message->setText(QString("loading item %1/%2").arg(current).arg(max));
   ui.progress->setValue(current / (float) max * 100.0);

   //ui.list->setRowCount(ui.list->rowCount() + 1);

   //int column = 0;
   //int row = ui.list->rowCount() - 1;

   //QLabel* imageLabel = nullptr;

   //if (contains(ID)) ui.list->setCellWidget(row, column++, new QLabel(QString::number(item.getID()), this));
   //if (contains(Image)) ui.list->setCellWidget(row, column++, (imageLabel = new QLabel(this)));
   //if (contains(Name)) ui.list->setCellWidget(row, column++, new QLabel(item.getName(), this));
   //if (contains(VendorValue)) ui.list->setCellWidget(row, column++, new QLabel(item.getVendorValue().toString(), this));

   //if (imageLabel) imageLabel->setPixmap(QPixmap::fromImage(item.getImage()).scaled(35, 35, Qt::KeepAspectRatio));
   //ui.list->resizeColumnsToContents();
}

