#include "stdafx.h"
#include "Dashboard.h"
#include "ItemList.h"

Dashboard& Dashboard::Instance()
{
   static Dashboard __dashboard;
   return __dashboard;
}

Dashboard::Dashboard()
{
    ui.setupUi(this);
    auto list = new ItemList(this);
    ui.view->layout()->addWidget(list);
}
