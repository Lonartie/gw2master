#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Dashboard.h"

class Dashboard: public QMainWindow
{
   Q_OBJECT

public: /*static methods*/

   static Dashboard& Instance();

private: /*ctors*/
   Dashboard();

private:
   Ui::DashboardClass ui;
};
