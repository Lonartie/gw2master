#pragma once

#include <QWidget>
#include "ui_CharacterViewer.h"

class CharacterViewer : public QWidget
{
    Q_OBJECT

public:
    CharacterViewer(QWidget *parent = Q_NULLPTR);
    ~CharacterViewer();

private:
    Ui::CharacterViewer ui;
};
