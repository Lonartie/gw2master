#pragma once
#include "stdafx.h"
#include "ui_ItemList.h"
#include "GW2Master.Core/Item.h"
#include "GW2Master.Core/Gold.h"

class ItemList: public QWidget
{
   Q_OBJECT;

public: /*typedefs*/

   enum ItemData
   {
      ID             = (1 <<  0),
      Name           = (1 <<  1),
      Image          = (1 <<  2),
      VendorValue    = (1 <<  3)
   };


public: /*ctors*/
   ItemList(QWidget* parent = Q_NULLPTR);
   ~ItemList() = default;

signals:

   void closing();

public: /*methods*/

   void setVisibleData(ItemData data);

protected:

   virtual bool eventFilter(QObject* watched, QEvent* event);

private: /*methods*/

   void fillList();
   void clearList() const;

   bool contains(ItemData data) const;

   void addItem(const GW2API::Item& item, std::size_t max);

private: /*members*/

   std::mutex waiter;
   std::vector<GW2API::Item> items;
   std::size_t current = 0;
   ItemData data = ItemData(ItemData::ID | ItemData::Name /*| ItemData::VendorValue | ItemData::Image*/);
   Ui::ItemList ui;
};
