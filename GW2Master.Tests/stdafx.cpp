#define BOOST_TEST_MODULE GW2MasterTests
#include "stdafx.h"

void init()
{
   static QCoreApplication app
   (
      boost::unit_test::framework::master_test_suite().argc,
      boost::unit_test::framework::master_test_suite().argv
   );
}
