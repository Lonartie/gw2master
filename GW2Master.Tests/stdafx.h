#pragma once

#include "GW2Master.Core/stdafx.h"
#include "TestMacros.h"

#include <boost/test/unit_test.hpp>

void init();

template<typename T>
inline std::size_t measure(std::function<void()> fn)
{
   auto begin = std::chrono::system_clock::now();
   fn();
   auto end = std::chrono::system_clock::now();
   return std::chrono::duration_cast<T>(end - begin).count();
}

namespace
{
   static inline const QString TEST_API_KEY = "4F3A4D44-AE18-EF4C-A499-36F5EA4150DF6D337273-8B96-4F74-A8DE-EE4BE5577DC3";
}
