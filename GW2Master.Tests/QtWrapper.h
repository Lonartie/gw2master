#pragma once
#include "stdafx.h"

class QtWrapper : public QObject
{
   Q_OBJECT;

public:

   QtWrapper(std::function<void()> fn);

public slots:

   void start();

private:

   std::function<void()> fn;
};