#include "stdafx.h"
#include "GW2Master.Core/API.h"
using namespace GW2API;

BOOST_AUTO_TEST_SUITE(TS_API);

BOOST_QT_TEST_CASE_BEGIN(NetworkRequest)
{
   auto reply = API::Instance().RequestJson("commerce/prices/19684");

   BOOST_REQUIRE(reply.contains("root"));
   auto root = reply["root"].toMap();

   BOOST_CHECK(root.contains("whitelisted"));
   BOOST_REQUIRE(root.contains("id"));
   BOOST_CHECK_EQUAL("19684", root["id"].toString());

   BOOST_REQUIRE(root.contains("buys"));
   auto buys = root["buys"].toMap();
   BOOST_CHECK(buys.contains("quantity"));
   BOOST_CHECK(buys.contains("unit_price"));

   BOOST_REQUIRE(root.contains("sells"));
   auto sells = root["sells"].toMap();
   BOOST_CHECK(sells.contains("quantity"));
   BOOST_CHECK(sells.contains("unit_price"));
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(ItemList)
{
   auto reply = API::Instance().RequestJson("items");

   BOOST_REQUIRE(reply.contains("root"));
   auto root = reply["root"].toList();

   auto size = root.size();
   BOOST_CHECK(1e3 < size);
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(ItemImage)
{
   const auto reply = API::Instance().RequestJson("items/30704");
   const auto path = QCoreApplication::applicationDirPath() + "/../../TestData/ItemImages/30704.png";
   const auto image = QImage(path, "PNG");

   BOOST_REQUIRE(reply.contains("root"));
   auto root = reply["root"].toMap();

   BOOST_REQUIRE(root.contains("icon"));
   auto icon = root["icon"].toString();

   auto test_image = API::Instance().RequestImage(icon);

   BOOST_CHECK(image == test_image);
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(RequestWithToken)
{
   const auto reply = API::Instance().RequestWithToken("account", TEST_API_KEY);

   BOOST_REQUIRE(reply.contains("root"));
   const auto root = reply["root"].toMap();

   BOOST_REQUIRE(root.contains("id"));
   BOOST_CHECK_EQUAL("BB000775-91CD-E911-81B6-95103C0D6492", root["id"].toString());

   BOOST_REQUIRE(root.contains("name"));
   BOOST_CHECK_EQUAL("Lonartie.6175", root["name"].toString());

   BOOST_REQUIRE(root.contains("created"));
   BOOST_CHECK_EQUAL("2019-09-02T14:54:00Z", root["created"].toString());
}
BOOST_QT_TEST_CASE_END();

BOOST_AUTO_TEST_SUITE_END();