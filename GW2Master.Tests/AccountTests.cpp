#include "stdafx.h"
#include "GW2Master.Core/Account.h"
#include "GW2Master.Core/API.h"
using namespace GW2API;

BOOST_AUTO_TEST_SUITE(TS_Account)

BOOST_QT_TEST_CASE_BEGIN(GetCharaters)
{
   AccountManager::Instance().clear();
   AccountManager::Instance().addAccount(TEST_API_KEY);
   auto accounts = AccountManager::Instance().getAccounts();

   BOOST_REQUIRE(1 == accounts.size());
   auto& account = accounts[0];

   BOOST_CHECK_EQUAL("Lonartie.6175", account.getAccountID());

   auto characters = account.getCharacters();

   BOOST_CHECK(std::find_if(characters.begin(), characters.end(), [](auto& ch) { return ch.getName() == "Suriarui"; }) != characters.end());
   BOOST_CHECK(std::find_if(characters.begin(), characters.end(), [](auto& ch) { return ch.getName() == "Suriarei"; }) != characters.end());
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(GetBankItems)
{
   AccountManager::Instance().clear();
   AccountManager::Instance().addAccount(TEST_API_KEY);
   auto accounts = AccountManager::Instance().getAccounts();

   BOOST_REQUIRE(1 == accounts.size());
   const auto& account = accounts[0];

   const auto items = account.getBankItems();
   const auto& first = items[0];
   const auto first_name = first.getName();

   BOOST_CHECK(items.size() > 0);
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(GetMaterialStockItems)
{
   AccountManager::Instance().clear();
   AccountManager::Instance().addAccount(TEST_API_KEY);
   auto accounts = AccountManager::Instance().getAccounts();

   BOOST_REQUIRE(1 == accounts.size());
   auto& account = accounts[0];

   const auto items = account.getMaterialStockItems();
   const auto& first = items[0];
   const auto first_name = first.getName();

   BOOST_CHECK(items.size() > 0);
}
BOOST_QT_TEST_CASE_END();

BOOST_AUTO_TEST_SUITE_END();