#pragma once
#include <QString>
#include <ostream>
#include "QtWrapper.h"

#define BOOST_QT_TEST_CASE_BEGIN(...)                                                                    \
BOOST_AUTO_TEST_CASE(__VA_ARGS__)                                                                        \
{                                                                                                        \
   init();                                                                                               \
   QTimer* timer = new QTimer();                                                                         \
   QEventLoop* loop = new QEventLoop();                                                                  \
   QtWrapper* wrapper = new QtWrapper([]() -> void                                                       \


#define BOOST_QT_TEST_CASE_END()                                                                         \
   );                                                                                                    \
   QObject::connect(timer, SIGNAL(timeout()), wrapper, SLOT(start()));                                   \
   QObject::connect(timer, SIGNAL(timeout()), loop, SLOT(quit()));                                       \
   timer->start();                                                                                       \
                                                                                                         \
   BOOST_CHECK_EQUAL(0, loop->exec());                                                                   \
   delete timer;                                                                                         \
   delete wrapper;                                                                                       \
   delete loop;                                                                                          \
}

namespace std
{
   inline std::ostream& operator<<(std::ostream& s, const QString& str)
   {
      return s << str.toUtf8().constData();
   }
}