#include "stdafx.h"
#include "QtWrapper.h"

QtWrapper::QtWrapper(std::function<void()> fn)
   : fn(fn)
{}

void QtWrapper::start()
{
   fn();
}
