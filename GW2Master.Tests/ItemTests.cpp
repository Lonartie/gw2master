#include "stdafx.h"
#include "GW2Master.Core/Item.h"
#include "GW2Master.Core/Cache.h"
using namespace GW2API;

BOOST_AUTO_TEST_SUITE(TS_Item)

BOOST_QT_TEST_CASE_BEGIN(All)
{
   Cache::Instance().clear();

   auto items = Item::All();
   BOOST_CHECK(50000 < items.size());
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(GetImage)
{
   Cache::Instance().clear();

   Item item(30704);
   QImage imag;

   auto first_call_time = measure<std::chrono::milliseconds>([&]() {imag = item.getImage(); });
   auto second_call_time = measure<std::chrono::milliseconds>([&]() {imag = item.getImage(); });

   const auto path = QCoreApplication::applicationDirPath() + "/../../TestData/ItemImages/30704.png";
   QImage ref_imag = QImage(path, "PNG");

   BOOST_CHECK(!imag.isNull());
   BOOST_CHECK(!ref_imag.isNull());
   BOOST_CHECK(imag == ref_imag);

   BOOST_CHECK(first_call_time > second_call_time);
   BOOST_CHECK(first_call_time > 100);
   BOOST_CHECK(second_call_time < 50);
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(GetVendorValue)
{
   Cache::Instance().clear();

   Item item(30704);
   Gold value = item.getVendorValue();

   BOOST_CHECK_EQUAL(Gold(10, 0, 0), value);
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(GetName)
{
   Cache::Instance().clear();

   Item item(30704);
   auto name = item.getName();

   BOOST_CHECK_EQUAL(QStringLiteral("D�sternis"), name);
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(AllInitializedAsync)
{
   return; // SKIP THIS TEST!
   Cache::Instance().clear();
   BOOST_TEST_MESSAGE("long operation coming...");
   std::size_t current = 0;
   std::mutex mut;

   auto cb = [&](auto& item, auto max)
   {
      mut.lock();
      current++;
      if (current % 100 == 0)
      {
         BOOST_TEST_MESSAGE(current / (float) max * 100.0 << " % (" << current << "/" << max << ") last item: " << item.getName());
      }
      mut.unlock();
   };

   auto thread = Item::AllInitializedAsync(cb);
   thread->wait();
   thread->deleteLater();
}
BOOST_QT_TEST_CASE_END();

BOOST_AUTO_TEST_SUITE_END();