#include "stdafx.h"
#include "GW2Master.Core/Account.h"
using namespace GW2API;

BOOST_AUTO_TEST_SUITE(TS_Character)

BOOST_QT_TEST_CASE_BEGIN(GetInventory)
{
   const auto acc = Account(TEST_API_KEY);
   
   const auto characters = acc.getCharacters();
   const auto& suriarui = *(std::find_if(characters.begin(), characters.end(), [](auto& ch) {return ch.getName() == "Suriarui"; }));
   const auto inventory = suriarui.getInventory();

   const auto& first = inventory[0];
   const auto first_name = first.getName();

   BOOST_CHECK(inventory.size() > 0);
}
BOOST_QT_TEST_CASE_END();

BOOST_QT_TEST_CASE_BEGIN(CountItems)
{
   const auto acc = Account(TEST_API_KEY);

   const auto characters = acc.getCharacters();
   const auto& suriarui = *(std::find_if(characters.begin(), characters.end(), [](auto& ch) {return ch.getName() == "Suriarui"; }));
   const auto count = suriarui.countItems(12138); // ID 12138 => Butter

   BOOST_CHECK(count > 25);
}
BOOST_QT_TEST_CASE_END();

BOOST_AUTO_TEST_SUITE_END();