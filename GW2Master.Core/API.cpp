#include "stdafx.h"
#include "API.h"
using namespace GW2API;

QVariantMap API::RequestJson(const QString& endpoint) const
{
   auto reply = Request(ROOT_URL + endpoint);
   if (!reply) return {};

   QString content = "{\"root\":" + QString::fromUtf8(reply->readAll()) + "}";
   reply->deleteLater();

   QJsonParseError jsonError;
   QJsonDocument flowerJson = QJsonDocument::fromJson(content.toUtf8(), &jsonError);
   QVariantMap list = flowerJson.object().toVariantMap();
   return list;
}

API& API::Instance()
{
   static API __instance;
   return __instance;
}

QImage API::RequestImage(const QString& url) const
{
   auto reply = Request(url);
   auto imag = QImage::fromData(reply->readAll(), "PNG");
   reply->deleteLater();
   return imag;
}

void API::Enqueue(std::function<void()> fn, QObject* obj)
{
   QMetaObject::invokeMethod(obj, fn);
}

QThread* API::Threaded(std::function<void()> fn)
{
   QtThreadFunctionWrapper* wrapper = new QtThreadFunctionWrapper(fn);
   //connect(wrapper, SIGNAL(finished()), wrapper, SLOT(deleteLater()), Qt::QueuedConnection);
   wrapper->start();
   return wrapper;
}

QVariantMap API::RequestWithToken(const QString& endpoint, const QString& token) const
{
   QNetworkAccessManager* network = new QNetworkAccessManager();
   QStringList parts = (ROOT_URL + endpoint).split("?");
   QString base_url = parts.first();

   parts.removeFirst();
   QUrlQuery query;

   for (auto& part : parts) query.addQueryItem(part.split("=")[0], part.split("=")[1]);

   QUrl url(base_url);
   url.setQuery(query);

   QNetworkRequest request = QNetworkRequest(url);
   request.setRawHeader("Authorization", ("Bearer " + token).toUtf8());

   auto reply = network->get(request);
   QNetworkReply::NetworkError err = QNetworkReply::NetworkError::NoError;

   QEventLoop loop;
   connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
   connect(reply, qOverload<QNetworkReply::NetworkError>(&QNetworkReply::error), this, [&](QNetworkReply::NetworkError code) { err = code; });
   loop.exec();
   network->deleteLater();
   if (!reply) return {};

   QString content = "{\"root\":" + QString::fromUtf8(reply->readAll()) + "}";
   reply->deleteLater();

   QJsonParseError jsonError;
   QJsonDocument flowerJson = QJsonDocument::fromJson(content.toUtf8(), &jsonError);
   QVariantMap list = flowerJson.object().toVariantMap();
   return list;
}

QNetworkReply* GW2API::API::Request(const QString& complete_endpoint) const
{
   QNetworkAccessManager* network = new QNetworkAccessManager();
   QStringList parts = complete_endpoint.split("?");
   QString base_url = parts.first();

   parts.removeFirst();
   QUrlQuery query;

   for (auto& part : parts) query.addQueryItem(part.split("=")[0], part.split("=")[1]);

   QUrl url(base_url);
   url.setQuery(query);

   QNetworkRequest request = QNetworkRequest(url);

   auto reply = network->get(request);
   QNetworkReply::NetworkError err = QNetworkReply::NetworkError::NoError;

   QEventLoop loop;
   connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
   connect(reply, qOverload<QNetworkReply::NetworkError>(&QNetworkReply::error), this, [&](QNetworkReply::NetworkError code) { err = code; });
   loop.exec();
   network->deleteLater();

   return reply;
}

QtThreadFunctionWrapper::QtThreadFunctionWrapper(std::function<void()> _fn)
   : __fn(_fn)
{}

void QtThreadFunctionWrapper::run()
{
   __fn();
}

