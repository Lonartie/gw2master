#pragma once
#include "stdafx.h"
#include "Character.h"

namespace GW2API
{
   class CORE_EXPORT Account
   {
   public: /*ctors*/
      Account(const QString& apiKey);
      Account(const QString& apiKey, const QVariantMap& map);

   public: /*methods*/
      QString getAccountID() const;
      std::vector<Character> getCharacters() const;
      std::vector<Item> getBankItems() const;
      std::vector<Item> getMaterialStockItems() const;

   private: /*methods*/
      const QVariantMap& getContent() const;

   private: /*members*/
      QString key;
      mutable QVariantMap content;

   };

   class CORE_EXPORT AccountManager : public QObject
   {
      Q_OBJECT;

   public: /*static methods*/
      static AccountManager& Instance();

   signals:
      void accountAdded(const Account& account);
      void accountRemoved(const Account& account);

   public: /*methods*/
      std::vector<Account> getAccounts();
      void addAccount(const QString& apiKey);
      void removeAccount(const QString& apiKey);
      void clear();

   private: /*ctors*/
      AccountManager();

   private: /*members*/
      QSettings settings;

   };
}