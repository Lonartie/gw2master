#include "stdafx.h"
#include "Account.h"
#include "API.h"
using namespace GW2API;

/*************************************************************************/
/*                            ACCOUNT MANAGER                            */
/*************************************************************************/

std::vector<Account> AccountManager::getAccounts()
{
   std::vector<Account> out;
   auto list = settings.value("ACCOUNTS").toList();
   for (auto& item : list) out.push_back(Account(item.toString()));
   return out;
}

void AccountManager::addAccount(const QString& apiKey)
{
   auto list = settings.value("ACCOUNTS").toList();
   if (list.contains(apiKey)) return;
   list << apiKey;
   settings.setValue("ACCOUNTS", list);
   settings.sync();
   emit accountAdded(Account(apiKey));
}

AccountManager::AccountManager()
   : settings(QCoreApplication::applicationDirPath() + "/accounts.ini", QSettings::IniFormat)
{
   settings.sync();
}

AccountManager& AccountManager::Instance()
{
   static AccountManager __manager;
   return __manager;
}

void AccountManager::clear()
{
   settings.clear();
   settings.sync();
}

void AccountManager::removeAccount(const QString& apiKey)
{
   auto list = settings.value("ACCOUNTS").toList();

   for (auto i = 0; i < list.size(); i++) 
      if (list[i].toString() == apiKey) 
      {
         list.removeAt(i); 
         emit accountRemoved(Account(apiKey));
         return; 
      }
}

/***************************************************************************/
/*                                  ACCOUNT                                */
/***************************************************************************/

Account::Account(const QString& apiKey)
   : key(apiKey)
{}

Account::Account(const QString& apiKey, const QVariantMap& map)
   : key(apiKey)
{
   auto root = map;
   if (map.contains("root")) root = map["root"].toMap();
   content = map;
}

std::vector<Character> Account::getCharacters() const
{
   auto reply = API::Instance().RequestWithToken("characters", key);
   auto list = reply["root"].toList();
   std::vector<Character> out;
   for (auto& item : list) out.push_back(Character(*this, key, item.toString()));
   return out;
}

QString Account::getAccountID() const
{
   auto& map = getContent();
   return map["name"].toString();
}

const QVariantMap& Account::getContent() const
{
   if (content.empty()) content = API::Instance().RequestWithToken("account", key)["root"].toMap();
   return content;
}

std::vector<Item> Account::getBankItems() const
{
   auto reply = API::Instance().RequestWithToken("account/bank", key);
   std::vector<Item> out;
   
   for (auto& item : reply["root"].toList())
      if (!item.isNull())
         if (item.toMap().contains("id"))
            if (item.toMap().contains("count"))
               for (auto i = 0; i < item.toMap()["count"].toInt(); i++)
                  out.push_back(Item(item.toMap()["id"].toULongLong()));

   return out;
}

std::vector<Item> Account::getMaterialStockItems() const
{
   auto reply = API::Instance().RequestWithToken("account/materials", key);
   std::vector<Item> out;

   for (auto& item : reply["root"].toList())
      if (!item.isNull())
         if (item.toMap().contains("id"))
            if (item.toMap().contains("count"))
               for (auto i = 0; i < item.toMap()["count"].toInt(); i++)
                  out.push_back(Item(item.toMap()["id"].toULongLong()));

   return out;
}
