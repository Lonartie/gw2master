#pragma once
#include "stdafx.h"
#include <ostream>

namespace GW2API
{
   class CORE_EXPORT Gold
   {
   public: /*ctors*/

      Gold(std::size_t amount = 0);
      Gold(std::size_t gold, std::size_t silver, std::size_t bronce);

   public: /*methods*/

      std::size_t gold() const;
      std::size_t silver() const;
      std::size_t bronce() const;

      QString toString() const;

   public: /*operators*/

      bool operator==(const Gold& o) const noexcept;
      bool operator!=(const Gold& o) const noexcept;
      bool operator>=(const Gold& o) const noexcept;
      bool operator>(const Gold& o) const noexcept;
      bool operator<(const Gold& o) const noexcept;
      bool operator<=(const Gold& o) const noexcept;

   private: /*members*/

      std::size_t amount;

   };
}

namespace std
{
   inline ostream& operator<<(ostream& s, const GW2API::Gold& g)
   {
      return s << "gold: " << g.gold() << " silver: " << g.silver() << " bronce: " << g.bronce();
   }
}