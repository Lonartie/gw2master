#pragma once
#include "stdafx.h"
#include "Gold.h"

namespace GW2API
{
   class CORE_EXPORT Item
   {
   public: /*static methods*/

      static std::vector<Item> All();
      static std::vector<Item> AllInitialized();
      static QThread* AllInitializedAsync(std::function<void(const Item&, std::size_t)> callback);

   public: /*ctors*/

      Item() = default;
      Item(std::size_t ID);
      Item(const QVariantMap& map);
      Item(const Item& o);
      Item(Item&& o);

   public: /*methods*/

      void init() const;

      std::size_t getID() const;

      QString getName() const;

      QImage getImage() const;

      Gold getVendorValue() const;

   public: /*operators*/

      Item& operator=(const Item& o) noexcept;
      Item& operator=(Item&& o) noexcept;

   private: /*methods*/

      void cacheIcon() const;
      void cacheName() const;
      QVariantMap getContent() const;

   private: /*ID methods*/

      QString ITEM_ENDPOINT() const;
      QString ICON_ID() const;
      QString NAME_ID() const;
      QString CONTENT_ID() const;

      static QString ITEMS_ID();

   private: /*static methods*/


      static void cacheItemList();

   private: /*members*/

      std::size_t ID;

   };
}