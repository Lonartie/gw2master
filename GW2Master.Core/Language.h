#pragma once
#include "stdafx.h"

namespace GW2API
{
   class Language
   {
   public: /*static methods*/

      static Language& Instance();

   public: /*methods*/

      QString getLanguage() const;

      void setLanguage(const QString& languageShort);

   private: /*ctors*/

      Language();

   private: /*members*/

      QSettings settings;

   };
}