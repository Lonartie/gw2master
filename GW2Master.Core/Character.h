#pragma once
#include "stdafx.h"
#include "Item.h"

namespace GW2API
{
   class Account;
   class CORE_EXPORT Character
   {
   public:
      Character(const Account& account, const QString& apikey, const QString& name);

   public:
      const QString& getName() const;
      std::vector<Item> getInventory() const;

      std::size_t countItems(std::size_t itemID) const;

   private: /*members*/
      QString key;
      QString name;
      const Account& account;
   };
}