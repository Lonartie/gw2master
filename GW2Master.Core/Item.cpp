#include "stdafx.h"
#include "Item.h"
#include "Cache.h"
#include "API.h"
#include "Language.h"
#include "MemoryCache.h"
using namespace GW2API;

/**************************************************************************/
/*                            STATIC FUNCTIONS                            */
/**************************************************************************/

namespace
{
   std::size_t CHUNK_SIZE = 200;
}

std::vector<Item> Item::All()
{
   std::vector<Item> out;
   auto reply = API::Instance().RequestJson("items");
   if (!reply.contains("root")) return {};
   auto root = reply["root"].value<QVariantList>();
   for (auto& item : root) out.push_back(Item(item.toULongLong()));
   return out;
}

std::vector<Item> Item::AllInitialized()
{
   // prepare lists
   auto reply = API::Instance().RequestJson("items");
   if (!reply.contains("root")) return{};
   auto root = reply["root"].value<QVariantList>();
   QList<QString> sroot;
   for (auto& item : root) sroot << QString::number(item.toULongLong());
   auto size = sroot.size();
   auto chunkSize = CHUNK_SIZE;
   std::size_t chunks = size / chunkSize + 1;
   std::vector<QThread*> threads;
   threads.resize(chunks);
   std::vector<Item> out;
   out.resize(sroot.size());
   auto out_ptr = &out;

#pragma omp parallel for
   for (long long i = 0; i < chunks; i++)
   {
      // downloading item list
      if (i * chunkSize >= size) continue;

      auto ids = sroot.mid(i * chunkSize, std::min<std::size_t>(chunkSize, size - (i * chunkSize))).join(",");
      auto clist = API::Instance().RequestJson("items?ids=" + ids + "?lang=" + Language::Instance().getLanguage());
      if (!clist.contains("root")) continue;
      auto croot = clist["root"].toList();

      // processing downloaded items
      threads[i] = API::Threaded([=]()
      {
         int c = 0;
         for (auto& item : croot) (*out_ptr)[i * chunkSize + (c++)] = Item(item.toMap());
      });
   }

   // wait for all
   for (auto thread : threads) { thread->wait(); thread->deleteLater(); }
   Cache::Instance().sync();
   return out;
}

QThread* Item::AllInitializedAsync(std::function<void(const Item&, std::size_t)> callback)
{
   return API::Threaded([callback]()
   {
      // prepare lists
      auto reply = API::Instance().RequestJson("items");
      if (!reply.contains("root")) return;
      auto root = reply["root"].value<QVariantList>();
      QList<QString> sroot;
      for (auto& item : root) sroot << QString::number(item.toULongLong());
      auto size = sroot.size();
      auto chunkSize = CHUNK_SIZE;
      std::size_t chunks = size / chunkSize + 1;
      std::vector<QThread*> threads;
      threads.resize(chunks);

#pragma omp parallel for
      for (long long i = 0; i < chunks; i++)
      {
         // downloading item list
         if (i * chunkSize >= size) continue;

         auto ids = sroot.mid(i * chunkSize, std::min<std::size_t>(chunkSize, size - (i * chunkSize))).join(",");
         auto clist = API::Instance().RequestJson("items?ids=" + ids + "?lang=" + Language::Instance().getLanguage());
         if (!clist.contains("root")) continue;
         auto croot = clist["root"].toList();

         // processing downloaded items
         threads[i] = API::Threaded([=]()
         {
            for (auto& item : croot)
               callback(Item(item.toMap()), size);
         });
      }

      // wait for all
      for (auto thread : threads) { thread->wait(); thread->deleteLater(); }
      Cache::Instance().sync();
   });
}

/***************************************************************************/
/*                                  CTORS                                  */
/***************************************************************************/

Item::Item(std::size_t ID)
   : ID(ID)
{}

Item::Item(const QVariantMap& map)
{
   auto root = map;
   if (map.contains("root")) root = map["root"].toMap();

   if (!MemoryCache::Instance().has(CONTENT_ID()))
      MemoryCache::Instance().set(CONTENT_ID(), root);

   ID = root["id"].toULongLong();
   auto name = root["name"].toString();

   if (!Cache::Instance().has(NAME_ID())) Cache::Instance().set(NAME_ID(), name);
}

Item::Item(const Item& o)
{
   ID = o.ID;
}

Item::Item(Item&& o)
{
   ID = std::move(o.ID);
}

/**************************************************************************/
/*                               FUNCTIONS                                */
/**************************************************************************/

void Item::init() const
{
   volatile auto _tmp = getContent();
}

QImage Item::getImage() const
{
   if (Cache::Instance().has(ICON_ID()))
      return Cache::Instance().get(ICON_ID()).value<QImage>();
   cacheIcon();
   if (Cache::Instance().has(ICON_ID()))
      return Cache::Instance().get(ICON_ID()).value<QImage>();
   return {};
}

void Item::cacheIcon() const
{
   auto& reply = getContent();

   if (!reply.contains("icon")) return;
   auto icon = reply["icon"].toString();

   auto icon_imag = API::Instance().RequestImage(icon);
   if (icon_imag.isNull()) return;

   Cache::Instance().set(ICON_ID(), icon_imag);
}

QString Item::ICON_ID() const
{
   return QString("ITEM_%1_ICON").arg(ID);
}

QString Item::ITEM_ENDPOINT() const
{
   return QString("items/%1?lang=%2").arg(ID).arg(Language::Instance().getLanguage());
}

QString Item::ITEMS_ID()
{
   return QString("ITEMS_ALL_IDS");
}

void Item::cacheItemList()
{
   auto reply = API::Instance().RequestJson("items");

   if (!reply.contains("root")) return;
   auto items = reply["root"].toList();

   if (items.size() == 0) return;

   Cache::Instance().set(ITEMS_ID(), items);
}

Gold Item::getVendorValue() const
{
   auto& reply = getContent();

   if (!reply.contains("vendor_value")) return {};
   return Gold(reply["vendor_value"].toULongLong());
}

QString Item::getName() const
{
   if (Cache::Instance().has(NAME_ID()))
      return QString::fromUtf8(Cache::Instance().get(NAME_ID()).toByteArray());
   cacheName();
   if (Cache::Instance().has(NAME_ID()))
      return QString::fromUtf8(Cache::Instance().get(NAME_ID()).toByteArray());
   return {};
}

void Item::cacheName() const
{
   auto& reply = getContent();

   if (!reply.contains("name")) return;
   auto name = reply["name"].toByteArray();

   Cache::Instance().set(NAME_ID(), name);
}

QString Item::NAME_ID() const
{
   return QString("ITEM_%1_NAME").arg(ID);
}

std::size_t Item::getID() const
{
   return ID;
}

QVariantMap Item::getContent() const
{
   if (MemoryCache::Instance().has(CONTENT_ID()))
      return MemoryCache::Instance().get(CONTENT_ID()).toMap();

   auto tmp = API::Instance().RequestJson(ITEM_ENDPOINT())["root"].toMap();
   MemoryCache::Instance().set(CONTENT_ID(), tmp);

   return tmp;
}

Item& Item::operator=(const Item& o) noexcept
{
   ID = o.ID;
   return *this;
}

Item& Item::operator=(Item&& o) noexcept
{
   ID = std::move(o.ID);
   return *this;
}

QString Item::CONTENT_ID() const
{
   return QString("ITEM_%1_CONTENT").arg(ID);
}
