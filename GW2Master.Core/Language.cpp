#include "stdafx.h"
#include "Language.h"
using namespace GW2API;

Language& Language::Instance()
{
   static Language __language;
   return __language;
}

Language::Language()
   : settings(QCoreApplication::applicationDirPath() + "/language.ini", QSettings::IniFormat)
{
   settings.sync();
   if (!settings.contains("CURRENT"))
      settings.setValue("CURRENT", "de");
   settings.sync();
}

QString Language::getLanguage() const
{
   return settings.value("CURRENT").toString();
}

void Language::setLanguage(const QString& languageShort)
{
   settings.setValue("CURRENT", languageShort);
}
