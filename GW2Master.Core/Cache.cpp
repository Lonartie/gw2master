#include "stdafx.h"
#include "Cache.h"
#include "Language.h"
using namespace GW2API;

Cache& Cache::Instance()
{
   static Cache __cache;
   return __cache;
}

Cache::Cache()
   : settings(QCoreApplication::applicationDirPath() + "/storage_" + Language::Instance().getLanguage() + ".ini", QSettings::Format::IniFormat)
{
   QMutexLocker locker(&mutex);
   settings.sync();
}

QVariant Cache::get(const QString& id, const QVariant& def /*= {}*/) const
{
   QMutexLocker locker(&mutex);
   return settings.value(id, def);
}

void Cache::set(const QString& id, const QVariant& val)
{
   QMutexLocker locker(&mutex);
   settings.setValue(id, val);
}

void Cache::clear()
{
   QMutexLocker locker(&mutex);
   settings.clear();
   settings.sync();
}

bool Cache::has(const QString& id) const
{
   QMutexLocker locker(&mutex);
   return settings.contains(id);
}

void Cache::sync()
{
   settings.sync();
}
