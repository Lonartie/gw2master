#include "stdafx.h"
#include "Character.h"
#include "API.h"
#include "Account.h"
using namespace GW2API;

Character::Character(const Account& account, const QString& apikey, const QString& name)
   : key(apikey)
   , name(name)
   , account(account)
{}

std::vector<Item> Character::getInventory() const
{
   auto reply = API::Instance().RequestWithToken("characters/" + name + "/inventory", key);
   auto root = reply["root"].toMap();
   std::vector<Item> out;

   for (auto& bag : root["bags"].toList())
      for (auto& item : bag.toMap()["inventory"].toList())
         if (item.toMap()["id"].toULongLong() != 0)
            for (auto i = 0; i < item.toMap()["count"].toInt(); i++)
               out.push_back(Item(item.toMap()["id"].toULongLong()));

   return out;
}

const QString& Character::getName() const
{
   return name;
}

std::size_t Character::countItems(std::size_t itemID) const
{
   auto inventory = getInventory();
   auto bank = account.getBankItems();
   auto materials = account.getMaterialStockItems();

   std::size_t count = 0;
   
   for (auto& item : inventory) if (item.getID() == itemID) count++;
   for (auto& item : bank) if (item.getID() == itemID) count++;
   for (auto& item : materials) if (item.getID() == itemID) count++;

   return count;
}
