#pragma once
#include "stdafx.h"

namespace GW2API
{
   class CORE_EXPORT API: public QObject
   {
      Q_OBJECT;

   public: /*static members*/

      static inline const QString ROOT_URL = "https://api.guildwars2.com/v2/";

   public: /*static methods*/

      static API& Instance();

      static void Enqueue(std::function<void()> fn, QObject* obj);

      static QThread* Threaded(std::function<void()> fn);

   public: /*methods*/

      QVariantMap RequestJson(const QString& endpoint) const;
      QImage RequestImage(const QString& endpoint) const;
      QVariantMap RequestWithToken(const QString& endpoint, const QString& token) const;

   private: /*ctors*/

      API() = default;
      ~API() = default;

   private: /*methods*/

      QNetworkReply* Request(const QString& complete_endpoint) const;
   };

   struct CORE_EXPORT QtThreadFunctionWrapper: public QThread
   {
      Q_OBJECT;
   public:
      QtThreadFunctionWrapper(std::function<void()> _fn);
   protected:
      virtual void run() override;
   private:
      std::function<void()> __fn;
   };
}