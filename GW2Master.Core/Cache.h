#pragma once
#include "stdafx.h"

namespace GW2API
{
   class CORE_EXPORT Cache
   {
   public: /*static methods*/

      static Cache& Instance();

   private: /*ctors*/

      Cache();

   public: /*methods*/

      void clear();

      void sync();

      QVariant get(const QString& id, const QVariant& def = {}) const;

      bool has(const QString& id) const;

      void set(const QString& id, const QVariant& val);

   private: /*members*/

      QSettings settings;
      mutable QMutex mutex;

   };
}