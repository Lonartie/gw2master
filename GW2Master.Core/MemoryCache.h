#pragma once
#include "stdafx.h"

namespace GW2API
{
   class CORE_EXPORT MemoryCache
   {
   public: /*static methods*/

      static MemoryCache& Instance();

   public: /*methods*/

      void clear();

      QVariant get(const QString& id, const QVariant& def = STATIC_EMPTY) const;

      bool has(const QString& id) const;

      void set(const QString& id, const QVariant& val);

   public: /*members*/

      static const QVariant STATIC_EMPTY;

   private: /*ctors*/

      MemoryCache() = default;

   private: /*members*/

      QVariantMap settings;
      mutable QMutex mutex;

   };
}
