#include "stdafx.h"
#include "MemoryCache.h"
using namespace GW2API;

const QVariant GW2API::MemoryCache::STATIC_EMPTY = {};

MemoryCache& MemoryCache::Instance()
{
   static MemoryCache __cache;
   return __cache;
}

void MemoryCache::clear()
{
   QMutexLocker lock(&mutex);
   settings.clear();
}

QVariant MemoryCache::get(const QString& id, const QVariant& def /*= {}*/) const
{
   QMutexLocker lock(&mutex);
   if (!settings.contains(id))
      return def;

   return settings[id];
}

bool MemoryCache::has(const QString& id) const
{
   QMutexLocker lock(&mutex);
   return settings.contains(id);
}

void MemoryCache::set(const QString& id, const QVariant& val)
{
   QMutexLocker lock(&mutex);
   if (!settings.contains(id))
   {
      settings.insert(id, val);
      return;
   }

   settings[id] = val;
}
