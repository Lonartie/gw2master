#include "stdafx.h"
#include "Gold.h"
#include <sstream>
using namespace GW2API;

Gold::Gold(std::size_t amount)
   : amount(amount)
{}

Gold::Gold(std::size_t gold, std::size_t silver, std::size_t bronce)
   : amount(bronce + (silver * 100) + (gold * 100 * 100))
{}

std::size_t Gold::gold() const
{
   return (amount) / (100 * 100);
}

std::size_t Gold::silver() const
{
   return (amount % (100 * 100)) / 100;
}

std::size_t Gold::bronce() const
{
   return amount % 100;
}

bool Gold::operator==(const Gold& o) const noexcept
{
   return amount == o.amount;
}

bool Gold::operator!=(const Gold& o) const noexcept
{
   return amount != o.amount;
}

bool Gold::operator>(const Gold& o) const noexcept
{
   return amount > o.amount;
}

bool Gold::operator>=(const Gold& o) const noexcept
{
   return amount >= o.amount;
}

bool Gold::operator<(const Gold& o) const noexcept
{
   return amount < o.amount;
}

bool Gold::operator<=(const Gold& o) const noexcept
{
   return amount <= o.amount;
}

QString Gold::toString() const
{
   return QString("gold: %1 silver: %2 bronce: %3").arg(gold()).arg(silver()).arg(bronce());
}
